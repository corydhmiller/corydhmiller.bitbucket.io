(function ($) {
  const data = $.getJSON('dist/assets/pricing.json')

  const utils = {
    addNumberCommas: num => new Intl.NumberFormat().format(num)
  }

  const updateView = data => {
    const num = parseInt($('.slider').val())
    const tiers = data.tiers
    const tier = tiers.find(el => num >= el.min && num <= el.max)
    updateExclusiveFeatureAvailability(tier.exclusive)
    updateSubscriberCount(num)
    updatePricingAmount(tier.price)
    updateSubscriberPlural(num)
  }

  const updateSubscriberCount = num => $('.subscriber__count').html(utils.addNumberCommas(num))

  const updateSubscriberPlural = num => $('.subscriber__plural').html(num === 1 ? 'subscriber' : 'subscribers')

  const updatePricingAmount = num => $('.price__amount').html(num)

  const updateExclusiveFeatureAvailability = unavailable => $('.feature--exclusive').toggleClass('feature--exclusive--unavailable', !unavailable)

  const loadDataAndUpdateView = () => {
    data.then(res => {
      updateView(res)
    })
  }

  const actionControllers = {
    updateSlider: el => {
      loadDataAndUpdateView()
    }
  }

  const $controllers = $('[data-controller]')

  $controllers.on('input change', event => {
    const _el = event.currentTarget
    const action = _el.getAttribute('data-controller')
    return actionControllers[action] ? actionControllers[action](_el) : console.error('Not a valid action.')
  })

  loadDataAndUpdateView()
})(jQuery)
