module.exports = {
    "extends": "standard",
    "env": {
        "commonjs": true,
        "es6": true,
        "jquery": true
    }
};